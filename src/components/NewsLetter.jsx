import { SendOutlined } from '@material-ui/icons/'
import React from 'react'
import styled from 'styled-components'


// IMPORT RESPONSIVE
import {mobile} from '../responsive'

const NewsLetter = () => {
  return (
    <Container>
            <Title>
                    Newsletter
            </Title>
            <Discription>
                Get timely update from your favorite products
            </Discription>
            <InputContainer>
                    <Input placeholder="Input your Email..."/>
                    <Button>
                            <SendOutlined/>
                    </Button>
            </InputContainer>

    </Container>
  )
}

export default NewsLetter


const Container =  styled.div`
        height:60vh;
        background:#ffcccc;
        display:flex;
        align-items: center;
        justify-content: center;
        flex-direction:column;


`

const Title = styled.h1`
    font-size:70px;
    margin-bottom: 20px;

`
const Discription =  styled.div`
        font-size: 24px;
        font-weight: 400;
        margin-bottom: 20px;
        ${mobile({textAlign: 'center'})};
`


const InputContainer = styled.div`
    width: 40%;
    height: 40px;
    background-color: white;
    display:flex;
    justify-content: space-between;
    border-top-right-radius: 10px 10px;
    border-bottom-right-radius: 10px 10px;
    ${mobile({width :"80px"})};
`

const Input =  styled.input`
    border:none;
    outline: none;
    flex:8;
    padding-left: 20px;
    
`

const Button = styled.button`
    flex:1;  
    outline: none;  
    border:none;
    background-color: teal;
    color:white;
    border-top-right-radius: 10px 10px;
    border-bottom-right-radius: 10px 10px;
`