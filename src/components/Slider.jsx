import { ArrowLeftOutlined, ArrowRightOutlined } from '@material-ui/icons'
import  {useState} from 'react'
import styled from 'styled-components'

// import image1  from '../images/pic1.png'

// IMPORT DATA FILE 
import {sliderItems } from "../data"


// IMPORT RESPONSIVEs
import {mobile} from '../responsive'








const Slider = () => {





  // define the tranform tramslateX in arrow function state
const [slideIndex, setSlideIndex] = useState(0);
// console.log("initial", slideIndex);

  // define the handleClick function
const handleClick  = (direction) => {

  if (direction === "left") {
    setSlideIndex(slideIndex > 0 ? slideIndex - 1 : 2)
    console.log("left", slideIndex)
  }else{
    setSlideIndex(slideIndex < 2 ? slideIndex  + 1 : 0)
    console.log("right", slideIndex)
  }


}

  return (
    <Container>

        <Arrow direction="left" onClick={() => handleClick("left")}>
            <ArrowLeftOutlined/>
        </Arrow>

      <Wrapper slideIndex={slideIndex}>
          {sliderItems.map((item, index) =>{
            return (
                        <Slide key={index} bg={item.bg}>
                        <ImageContainer>
                             <Image src={item.img}alt="image"/>
                        </ImageContainer>
                        <InfoContainer>
                              <Title>{item.title}</Title>
                              <Description>{item.desc}</Description>
                              <Buttom>SHOW NOW</Buttom>
                        </InfoContainer>  
        
                  </Slide>
         ) })}


        

      </Wrapper>
      
        <Arrow direction="right" onClick={() => handleClick("right")}>
            <ArrowRightOutlined/>
        </Arrow>

    </Container>
  )
}

export default Slider



const Container = styled.div`
    width: 100%;
    height:100vh;
    display: flex;
    // background:red;
    position: relative;
    overflow: hidden;
    ${mobile({display :"none"})};


`

const Wrapper = styled.div` 
  height:100%;
  margin: 0 auto;
  display:flex;
   transform:translateX(${props => props.slideIndex * -111.5}vw);
   position:relative;

  // == animate ==
  transition: all 1.5s ease

  
`

const Slide = styled.div`
  display:flex;
  align-items:center
  width: 100vw;
  height: 100vh;
  flex:1;

  // used props
  background-color: #${props => props.bg}



`


const ImageContainer = styled.div`
  flex:1;
  height: 100%;
  width:100vh;
//  background: red;
 position:relative;


`
const Image  = styled.img`
  height: 80%;
  position: absolute;
  right:-200px;
  
  
`

const InfoContainer = styled.div`
    flex:3;
    padding:150px;
    margin: 0 auto 0 20rem;
    // background: yellow;
    position:relative;

 
`

const Title = styled.h1`
    font-size : 70px;
`

const Description = styled.h1`
  margin: 50px 0px;
  font-size: 20px;
  font-weight:500;
  letter-spacing: 3px;
  


`

const Buttom = styled.button`
    padding:10px;
    font-size: 20px;
    background-color: transparent;
    cursor:pointer;

`

const Arrow = styled.div`
        width: 50px;
        height:50px;
        background-color: 	#BDB76B;
        border-radius: 50%;
        display:flex;
        align-items:center;
        justify-content:center;
        position:absolute;
        top:0;
        bottom:0;
        left: ${props => props.direction === "left" && "10px"};
         right: ${props => props.direction === "right" && "10px"};
        margin: auto;
        cursor:pointer;
        opacity:0.5;
        z-index:2;
        
`

