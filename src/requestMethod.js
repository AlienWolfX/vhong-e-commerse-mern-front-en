import axios from "axios";


const BASE_URL = "http://localhost:5000/api/";
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGQwZWIwOGZmYzY1NGQ2NGNmYmU5ZCIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY3NTY5NTUxOSwiZXhwIjoxNjc1OTU0NzE5fQ.PmnegQgFG-nLdJIN0UTj6KqovDNMgaUYnWyX9mnZFTY";


export const publicRequest  = axios.create({
    baseURL: BASE_URL,
})


export const userRequest = axios.create({
    baseURL: BASE_URL,
    headers: {
        token: `Bearer ${TOKEN}` 
    }
})
