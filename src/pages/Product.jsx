

import { Add, Remove } from '@material-ui/icons'
import React,{useState,useEffect} from 'react'
import styled from 'styled-components'
import Annoucment from '../components/Annoucment'
import Footer from '../components/Footer'
import Navbar from '../components/Nabvar'
import NewsLetter from '../components/NewsLetter'
import { mobile } from '../responsive'
import {useLocation} from 'react-router-dom'
import { publicRequest } from '../requestMethod';
import {ADD_PRODUCT} from '../redux/cartRedux';
import { useDispatch } from 'react-redux'

const Product = () => {
        // DEFINE TO DESPATCH THE PAYLOAD
    const dispatch = useDispatch();


    // define the URL path of category 
   const location = useLocation();
   const productId = location.pathname.split("/" || " ")[2];


   // DEFINE THE PRODUCT ID AND THEIR COLOR ,SIZE AND DESCRIPTION
    const [product, setProduct] = useState({})
        console.log(product);
    //DEFINE THE QUANTITY PRODUCT STATE
    const [quantity, setQuantity] = useState(1);
    // DEFINE THE COLOR STATE
    const [color, setColor] = useState("");
    //DEFINE THE SIZE STATE
    const [size, setSize] = useState("");



    //DEFINE THE BUTTOM INC AND DEC FUNCTION  QUANTITY
    const handleQuntity = (type) => {
    
        if (type === "dec") {
                
            quantity > 1 &&  setQuantity(prev => prev -1) 
        }else{
            setQuantity(prev => prev + 1 )
        }
    }
    
    //DEFINE THE FUNCTION FOR ADD TO CART USE REDUX TOOKIT TO MANAGE THE STATE APPLICATION 
    const handleClick = () => {
        // ES6 introduce the way to  put the value if the same name the property and value only name of the obejct which is overwrite 
                //Ex: product:product --> same as product,
         dispatch(ADD_PRODUCT({
               ...product,
                quantity,
                size,
                color
            }))
    }

   useEffect(() => {
      const getProducts = async () => {
          try {
           const response = await publicRequest.get('/product/find/'+productId);
            // console.log(response.data.color)
            setProduct(response.data)
          } catch (error) {
            
          }

      }
      getProducts()
      
    
   }, [productId])
//    console.log(product.title)
   
  return (
    <Container>
            <Navbar/>
            <Annoucment/>
                <Wrapper>
                        <ImgContainer>
                                <Image src={product.image}/>
                        </ImgContainer>

                        <InfoContainer>
                                <Title>{product.title}</Title>
                                <Description>{product.desc}</Description>
                                <Price>${product.price}</Price>
                                <FilterContainer>
                                        <Filter>
                                            <FilterTitle>Color</FilterTitle>
                                              {(product.color || []).map((c,index) =>(
                                                    <FilterColor colors={c} key={index} onClick={() => setColor(c)}/>
                                            ))}   
                                           
                                        </Filter>

                                        <Filter>

                                                        <FilterTitle>Size</FilterTitle>
                                                             <FiltersizesSelecst onClick={(e) => setSize(e.target.value)}>
                                                                {(product.size || []).map((s,index) =>(
                                                                    <FilterSizeOption key={index}>
                                                                            {s}
                                                                    </FilterSizeOption> 
                                                                    
                                                                ))}
                                                                 
                                                            </FiltersizesSelecst> 

                                        </Filter>
                                </FilterContainer>

                        <AddContainer>
                                <AmountContainer>
                                            <Remove onClick={() =>handleQuntity("dec")}/>
                                                <Amount>{quantity}</Amount>
                                            <Add onClick={() =>handleQuntity("inc")}/>
                                  
                                </AmountContainer>
                                <Button onClick={handleClick}>ADD TO CART</Button>
                        </AddContainer>

                        </InfoContainer>
                </Wrapper>
            <NewsLetter/>
            <Footer/>
    </Container>
  )
}

export default Product

 
const Container =  styled.div`


`

const Wrapper = styled.div`
    padding:50px;
    display:flex;
    ${mobile({padding:"10px", flexDirection:"column"})};

`


const ImgContainer = styled.div`
        flex:1;
`

const Image = styled.img`
    width:100%;
    height: 90vh;
    object-fit:cover;
    ${mobile({height:"40vh"})};


`


const InfoContainer = styled.div`
flex:1;
padding: 0 50px;
${mobile({padding:"10px"})};
`

const Title = styled.h1`
    font-weight:400;

`
const Description = styled.div`
    margin: 20px 0px;

`

const Price = styled.p`
    font-weight:100;
    font-size:40px;
`


const FilterContainer = styled.div`
display:flex;
justify-content:space-between;
width:50%;
margin-top: 20px;
${mobile({width:"100%"})};
`



const Filter = styled.div`
    display:flex;
    align-items: center;
`


const FilterTitle = styled.span`
    font-size: 20px;
    font-weight:200;
    `

const FilterColor = styled.div`
    width:20px;
    height: 20px;
    border-radius:50%;
    background-color: ${props => props.colors};
    margin: 0 5px;
    cursor: pointer;
`

const FiltersizesSelecst = styled.select`
    margin-left: 10px;
    padding: 5px;


`

const FilterSizeOption = styled.option`` 

const AddContainer = styled.div`
    margin: 25px 0;
    width:50%;
    display:flex;
    align-items: center;
    justify-content:space-between;
    ${mobile({width:"100%"})};


`
const AmountContainer = styled.div`
    display: flex;
    align-items:center;
    font-weight: 700;
`

const Amount = styled.span`
    width:30px;
    display:flex;
    align-items:center;
    justify-content: center;
    border-radius:10px;
    border: 1px solid teal;
`


const Button = styled.button`
    padding: 15px;
    border: 1px solid teal;
    background-color: white;
    cursor:pointer;

    &:hover{

        background-color: teal;
        color:white;
    
    }

`

