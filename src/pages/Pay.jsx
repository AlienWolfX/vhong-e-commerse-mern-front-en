
import React,{useState,useEffect} from 'react';
import Styled from 'styled-components/'
import { useNavigate } from "react-router-dom";
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios'

// THE  KEY FOR  UI STRIPE PLATFROM THIS  KEY IS FOR CLIENT SERVER 
const KEY = "pk_test_51MXmzxCQfJnPmnPjwYl1lMgPx9UIcdFyNjSyxLfq7cG6Eiry2n8SMhxrua9C785lVDi5uIisHlqxf2bjY8Qynsox00pnyQfBBJ"

const Pay = () => { 
  const navigate = useNavigate();

    // define the token fro stripe
  const [stripeToken , setTokenStripe] =  useState(null)
    
  //difine the token 
const onToken = (token) => {
  setTokenStripe(token)
}

useEffect(() => {
  // console.log("stripeToken:",stripeToken ? true : false)
    const makeRequest = async () => {
      try {
      const response = await axios.post("http://localhost:5000/api/checkout/payment", {
        tokenId:stripeToken.id,
        amount: 2000,
      })
      console.log("response:",response.data)
      console.log(response.data);
      navigate("/sucess")
      } catch (error) {
        console.log(error);
      }

    }
    

     stripeToken && makeRequest()


  },[stripeToken, navigate])
    


  return (
    <Container>
        Thank you For Purchasing the items
      <Wrapper>
            <ButtonText>
                  Here the payment Page
            </ButtonText>



                    {stripeToken ? <span>Processing Pleas wait...</span> : (
                      <StripeCheckout name="Vhong Shop" image="https://media.istockphoto.com/id/874045548/vector/shirt-icon.jpg?s=612x612&w=0&k=20&c=ZJCxsCczemu1XhYRMDCByrYdwotBESuFdC5tkGf1a6g="
                      billingAddress 
                      shippingAddress
                      description='Your Total is $20'
                      amount={2000} 
                      token={onToken}
                      stripeKey= {KEY}
                
                      >
            
                  <ButtonPayment>
                      Payment Button
                  </ButtonPayment>
                
           </StripeCheckout>
      )}
 
     
      </Wrapper>

    </Container>
  )
}

const Container = Styled.div`
 width: 100%;
 height: 100vh;
 display:flex;
 flex-direction: column;
 align-items: center;
 justify-content:center;

`
const Wrapper = Styled.div`
display:flex;
align-items:center;
flex-direction: column;
margin: 20px 0 ;

`


const ButtonText = Styled.p`  
  font-size: 24px;
  font-weight: 800;
  margin: 20px 0;

`


const ButtonPayment = Styled.button`
  color: white;
  background:teal;
  font-size: 22px;
  padding: 10px;
  border-radius: 1rem;
  cursor:pointer;

`

export default Pay
